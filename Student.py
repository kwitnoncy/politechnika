

class Student:
    def __init__(self, first_name: str, last_name: str, student_id: int, class_name: str, grade: int):
        self.first_name = first_name
        self.last_name = last_name
        self.id = student_id
        self.class_name = class_name
        self.grade = grade

    def get_student(self):
        return self.first_name, self.last_name, self.id, self.class_name, self.grade

    def get_id(self):
        return self.id

    def get_class_name(self):
        return self.class_name

    def get_grade(self):
        return self.grade
