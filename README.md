# PUT Management Application

## Overview

This is simple REST (or wannabe-REST) application that connects to University database 
and provides users request data (such as attendance or grades).

## Description

Project started to help checking attendance. The whole system was built around embedded
system, which read students' card ID and send it to database.

This version has no connection to any embedded system (yet), but all the date can 
be given manually via this application.

## Tools/technologies

1. Pycharm (to write application code):
    - Python(3.7, 64-bit)
    - Flask (1.0.3)
    - json 
    - pyodbc (4.0.26)
2. DataGrip (to check database):
    - SQL Server database
3. Postman (to test)
4. My own python client (also to test)

## How to run
  
Server works on localhost so far. I suggest to import this project to pycharm which quite clearly 