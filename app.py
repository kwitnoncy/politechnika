from flask import Flask, render_template, request, redirect, url_for
from typing import List, Tuple, Any
import pyodbc
import os
import json
import datetime
import time

import Student


# TODO:
#     - README.md
#     - CATEGORIES i CLASSES_NAMES pobierac z bazy danych

SERVER_ADDR = ''
DATABASE_NAME = ''
USER_NAME = ''
USER_PASS = ''
LECTURER_ID = 0
CATEGORIES = ['Wyklad', 'Cwiczenia', 'Laboratorium', 'Projekt']
CLASSES_NAMES = ['Programowanie', 'Kompilatory', 'Automatyka', 'Robotyka']

app = Flask(__name__)

"""
    config_db() function configures database connection from config.json file
    up to date
"""
def config_db():
    global SERVER_ADDR, DATABASE_NAME, USER_NAME, USER_PASS
    json_file = open(os.getcwd() + '\\config.json', mode='r', encoding='utf-8')
    database_config = json.loads(str(json_file.read(-1)))
    SERVER_ADDR = database_config["SERVER_ADDR"]
    DATABASE_NAME = database_config["DATABASE_NAME"]
    USER_NAME = database_config["USER_NAME"]
    USER_PASS = database_config["USER_PASS"]
    json_file.close()


"""
    connect() return pyodbc.Connection object to database configured by config_db() function
    up to date
"""
def connect() -> pyodbc.Connection:
    global SERVER_ADDR, DATABASE_NAME, USER_NAME, USER_PASS
    config_db()
    conn = pyodbc.connect('DRIVER={SQL Server};'
                          'SERVER=' + SERVER_ADDR + ';'
                          'DATABASE=' + DATABASE_NAME + ';'
                          'UID=' + USER_NAME + ';'
                          'PWD=' + USER_PASS)
    return conn


"""
    get_class_lec_id(class_name: str) function returns tuple
    up to date
"""
def get_class_lec_id(class_name: str) -> tuple:
    classes, _ = get_table('classes')

    if not classes == []:
        for class_ in classes:
            if class_[2] == class_name:
                return class_[0], class_[1]

    return None, None


"""
    sends students grade to database
    student is object that contains proper data
    NOT up to date
"""
def send_students_grade(student: Student) -> int:
    global LECTURER_ID
    class_id, lecturer_id = get_class_lec_id(student.get_class_name())

    if class_id is not None and lecturer_id is not None:
        query_file = open(os.getcwd() + '\\queries\\insert_grade.sql', mode='r', encoding='utf-8')
        connection = connect()
        cursor = connection.cursor()

        query = str(query_file.read(-1) + '(' + str(student.get_id()) + ', ' + str(lecturer_id) + ', ' + \
                str(class_id) + ', \'' + str(datetime.datetime.today().strftime('%Y-%m-%d')) + '\', ' + \
                str(student.get_grade()) + ')')

        try:
            cursor.execute(query)
            connection.commit()
            connection.close()
        except pyodbc.ProgrammingError:
            print(query)
            return -1

        return 0

    return -1


"""
    sends attendance to database
    up to date
"""
def send_attendance(fname: str, sname: str, lecturer_id: int, class_name: str) -> int:
    class_id, _ = get_class_lec_id(class_name)

    students, _ = get_table('students')

    for student in students:
        if student[1] == fname and student[2] == sname:
            student_id = student[0]
            break
    else:
        return -2

    if class_id is not None and lecturer_id is not None:
        query_file = open(os.getcwd() + '\\queries\\insert_attendance.sql', mode='r', encoding='utf-8')
        connection = connect()
        cursor = connection.cursor()

        query = query_file.read(-1) + '(' + str(student_id) + ', ' + str(lecturer_id) + ', ' + str(class_id) + ', \'' +\
                str(datetime.datetime.today().strftime('%Y-%m-%d')) + '\')'

        cursor.execute(query)
        connection.commit()
        connection.close()
        return 0

    return -1


"""
    returns table which name was given as parameter
    WARNING works only on tables which have corresponding SQL query
"""
def get_table(table_name: str) -> Tuple:
    try:
        connection = connect()
        cursor = connection.cursor()
        sql_file = open(os.getcwd() + '\\queries\\'+table_name+'_list.sql', mode='r', encoding='utf-8')
        query = str(sql_file.read(-1))
        cursor.execute(query)
        table = cursor.fetchall()
        sql_file.close()
    except FileExistsError:
        print("Wrong table name")
        return ()
    except pyodbc.DataError:
        print("Database Error")
        return ()
    except:
        print("Some other weird error")
        return ()

    return table, cursor.description


"""
    main endpoint redirects to /index
"""
@app.route('/')
def hello():
    return redirect('/index')


"""
    /index endpoint
    here is implemented pseudo-log-in mechanism
    gets info about user (first and last name) and check if he/she is a lecturer
"""
@app.route('/index', methods=['GET', 'POST', 'PUT'])
def hello_world():
    global LECTURER_ID

    if request.method == 'GET':
        return render_template('index.html')
    elif request.method == 'POST':
        first_name = request.form['first_name']
        last_name = request.form['last_name']

        lecturers, _ = get_table('lecturers')
        if not lecturers == []:
            for lecturer in lecturers:
                if lecturer[1] == first_name and lecturer[2] == last_name:
                    LECTURER_ID = lecturer[0]
                    return redirect(url_for('home'), code=302)
                else:
                    error = 'nie jestes wykladowca'
                    return render_template('index.html', error=error)
        else:
            error = 'blad serwera'
            return render_template('index.html', error=error)
    elif request.method == 'PUT':
        data = request.json
        first_name = data['first_name']
        last_name = data['last_name']

        lecturers, _ = get_table('lecturers')
        if not lecturers == []:
            for lecturer in lecturers:
                if lecturer[1] == first_name and lecturer[2] == last_name:
                    LECTURER_ID = lecturer[0]
                    return json.dumps({"json": "ok", "method": "PUT"})
                else:
                    return json.dumps({"json": "not ok", "method": "PUT", "cause": "No Lecturer"})
        else:
            pass


"""
    /home endpoint
    user is given possibility to pick what he/she wants to do 
"""
@app.route('/home')
def home():
    return render_template('home.html')


"""
    /attendance/student/id endpoint
    endpoint to get all attendance from students (who is identified by given id) 
"""
@app.route('/attendance/student/<int:student_id>', methods=['GET'])
def attendance_by_student_id(student_id):
    if request.content_type == 'application/json':
        if student_id <= 0:
            return json.dumps({"json": "attendance/student/id",
                               "code": "error",
                               "cause": "wrong id",
                               "method": "GET"})

        attendance_table, description = [], []
        attendance_table, description = get_table('attendance')
        students_table, _ = get_table('students')

        data = []
        for row in attendance_table:
            x = {
                "AttendanceID": row[0],
                "StudentID": row[1],
                "OccurenceID": row[2],
                "DateOfOccurence": str(row[3])
            }
            if row[1] == student_id:
                data.append(x)

        return json.dumps({"json": "attendance",
                            "code": "ok",
                            "method": "GET",
                            "all": "1",
                            "data": data})


"""
    /attendance/id endpoint
    endpoint to get attendance by its id
"""
@app.route('/attendance/<int:attendance_id>', methods=['GET'])
def attendance_by_id(attendance_id):
    if request.content_type == 'application/json':
        data = request.json
        attendance_table, description = get_table('attendance')

        data = []
        for row in attendance_table:
            x = {
                "AttendanceID": row[0],
                "StudentID": row[1],
                "OccurenceID": row[2],
                "DateOfOccurence": str(row[3])
            }
            if row[0] == attendance_id:
                data.append(x)


        return json.dumps({"json": "attendance/id",
                            "code": "ok",
                            "method": "GET",
                            "data": data})


"""
    /attendance endpoint
    endpoint from which user can add, get and delete date about attendance
"""
@app.route('/attendance', methods=['GET', 'POST', 'DELETE'])
def attendance():
    global CLASSES_NAMES, CATEGORIES
    group_id = -1
    occurrence_id = -1
    student_id = -1

    if request.method == 'GET':
        if request.content_type == 'application/json':
            data = request.json
            all_table = bool(int(data['all']))
            attendance_table, description = get_table('attendance')

            if not all_table:

                first_name = data['first_name']
                last_name = data['last_name']
                students_table, _ = get_table('students')

                for student in students_table:
                    if student[1] == first_name and student[2] == last_name:
                        student_id = int(student[0])

                if student_id == -1:
                    if request.content_type == 'application/json':
                        return json.dumps({"json": "get attendance",
                                           "code": "error",
                                           "cause": "student do not exists",
                                           "method": "GET"})

            data_all = []
            data_not_all = []
            for row in attendance_table:
                x = {
                    "AttendanceID": row[0],
                    "StudentID": row[1],
                    "OccurenceID": row[2],
                    "DateOfOccurence": str(row[3])
                }
                if not all_table:
                    if str(x['StudentID']) == str(student_id):
                        data_not_all.append(x)

                data_all.append(x)

            if all_table:
                return json.dumps({"json": "attendance",
                                   "code": "ok",
                                   "method": "GET",
                                   "all": "1",
                                   "data": data_all})
            else:
                return json.dumps({"json": "attendance",
                                   "code": "ok",
                                   "method": "GET",
                                   "all": "0",
                                   "data": data_not_all})
        else:
            return render_template('attendance.html', Classes_names=CLASSES_NAMES, Categories=CATEGORIES)
    elif request.method == 'POST':
        if request.content_type == 'application/json':
            data = request.json
            student_f_name = data['first_name']
            student_s_name = data['last_name']
            class_category = int(data['category'])
            class_id = int(data['class_id'])
            lecturer_id = int(data['lecturer_id'])
        else:
            try:
                student_f_name = str(request.form["s_fname"])
                student_s_name = str(request.form["s_sname"])
                lecturer_id = int(request.form["lecturer_id"])
                class_id = int(request.form["class_id"])
                class_category = int(request.form["category_id"])
            except ValueError:
                error = 'Wrong input'
                return render_template('attendance.html', error=error, Classes_names=CLASSES_NAMES, Categories=CATEGORIES)

        students, _ = get_table('Students')

        for student in students:
            if student[1] == student_f_name and student[2] == student_s_name:
                group_id = int(student[3])
                student_id = int(student[0])

        if group_id == -1 or student_id == -1:
            if request.content_type == 'application/json':
                return json.dumps({"json": "attendance",
                                   "code": "error",
                                   "cause": "group or student do not exists",
                                   "method": "POST"})
            else:
                error = 'Wrong input: group or student do not exists'
                return render_template('attendance.html', error=error, Classes_names=CLASSES_NAMES, Categories=CATEGORIES)

        occurrences, _ = get_table('occurence')

        for occ in occurrences:
            if occ[3] == group_id and occ[4] == class_category and occ[5] == class_id and occ[6] == lecturer_id:
                occurrence_id = occ[0]

        if occurrence_id == -1:
            if request.content_type == 'application/json':
                return json.dumps({"json": "attendance",
                                   "code": "error",
                                   "cause": "occurrence does not exists",
                                   "method": "POST"})
            else:
                error = 'occurrence does not exists'
                return render_template('attendance.html', error=error, Classes_names=CLASSES_NAMES, Categories=CATEGORIES)

        connection = connect()
        cursor = connection.cursor()
        query = open(os.getcwd() + '\\queries\\insert_attendance.sql', mode='r', encoding='utf-8').read(-1)
        query = query + '(' + str(student_id) + ', ' + str(occurrence_id) + ', CAST(\'' + \
                str(datetime.datetime.today().strftime('%Y-%m-%d')) + '\' as datetime))'

        cursor.execute(query)
        connection.commit()
        connection.close()

        if request.content_type == 'application/json':
            return json.dumps({"json": "attendance",
                               "code": "ok",
                               "method": "POST"})
        else:
            return render_template('attendance.html', Classes_names=CLASSES_NAMES, Categories=CATEGORIES)
    elif request.method == 'DELETE':
        if request.content_type == 'application/json':
            data = request.json
            delete_all = bool(int(data['all']))
            if not delete_all:
                student_f_name = data['first_name']
                student_s_name = data['last_name']
                class_category = int(data['category'])
                class_id = int(data['class_id'])
                lecturer_id = int(data['lecturer_id'])

                print(student_f_name, student_s_name, class_category, class_id, lecturer_id)
            else:
                try:
                    connection = connect()
                    cursor = connection.cursor()
                    cursor.execute("DELETE FROM Attendance where AttendanceID >= 1")
                    connection.commit()
                    connection.close()
                except pyodbc.Error:
                    return json.dumps({"json": "delete attendance",
                                       "code": "error",
                                       "cause": "Database error",
                                       "method": "DELETE"})

        return json.dumps({"json": "delete attendance",
                           "code": "ok",
                           "method": "DELETE"})


"""
    /grades/student/id endpoint
    endpoint to get all grades from students (who is identified by given id) 
"""
@app.route('/grades/student/<int:url_id>', methods=['GET', 'POST', 'PUT', 'DELETE'])
def grades_by_student_id(url_id: int):
    grades_table, _ = get_table('grades')
    data = []
    for row in grades_table:
        if row[1] == url_id:
            x = {
                "GradeID": row[0],
                "StudentID": row[1],
                "OccurenceID": row[2],
                "DateOfLecture": str(row[3]),
                "Grade": row[4]
            }
            data.append(x)

    return json.dumps({"json": "get grades",
                       "code": "ok",
                       "method": "GET",
                       "all": "grade_id",
                       "data": data})


"""
    /grades/id endpoint
    endpoint to get grade by its id
"""
@app.route('/grades/<int:grade_id>', methods=['GET'])
def grades_by_id(grade_id: int):
    grades_table, _ = get_table('grades')
    for row in grades_table:
        if row[0] == grade_id:
            x = {
                "GradeID": row[0],
                "StudentID": row[1],
                "OccurenceID": row[2],
                "DateOfLecture": str(row[3]),
                "Grade": row[4]
            }
            return json.dumps({"json": "get grades",
                              "code": "ok",
                               "method": "GET",
                               "all": "grade_id",
                               "data": x})

    return json.dumps({"json": "get grades",
                       "code": "error",
                       "method": "GET",
                       "all": "grade_id",
                       "cause": "no grade with given key"})


"""
    /grades endpoint
    endpoint from which user can add, get and delete date about grades
"""
@app.route('/grades', methods=['GET', 'POST', 'PUT', 'DELETE'])
def grades():
    student_id = -1
    group_id = -1
    occurrence_id = -1
    grade_id = -1
    get_student = False

    if request.method == 'GET':
        if request.content_type == 'application/json':
            data = request.json
            all_table = bool(int(data['all']))
            grades_table, description = get_table('grades')

            if not all_table and not get_student:
                first_name = data['first_name']
                last_name = data['last_name']
                students_table, _ = get_table('students')

                for student in students_table:
                    if student[1] == first_name and student[2] == last_name:
                        student_id = int(student[0])

                if student_id == -1:
                    if request.content_type == 'application/json':
                        return json.dumps({"json": "get grades",
                                           "code": "error",
                                           "cause": "student do not exists",
                                           "method": "GET"})

            data_all = []
            data_not_all = []
            if grade_id > 0:
                for row in grades_table:
                    if row[0] == grade_id:
                        x = {
                            "GradeID": row[0],
                            "StudentID": row[1],
                            "OccurenceID": row[2],
                            "DateOfLecture": str(row[3]),
                            "Grade": row[4]
                        }
                        data_all.append(x)
                return json.dump({"json": "get grades",
                                  "code": "ok",
                                  "method": "GET",
                                  "all": "grade_id",
                                  "data": data_all})
            elif get_student:
                students_table, _ = get_table('students')

                for student in students_table:
                    if student[0] == student_id:
                        x = {
                            "GradeID": student[0],
                            "StudentID": student[1],
                            "OccurenceID": student[2],
                            "DateOfLecture": str(student[3]),
                            "Grade": student[4]
                        }
                        data_all.append(x)

                return json.dump({"json": "get grades",
                                  "code": "ok",
                                  "method": "GET",
                                  "all": "student_id",
                                  "data": data_all})

            for row in grades_table:
                x = {
                    "GradeID": row[0],
                    "StudentID": row[1],
                    "OccurenceID": row[2],
                    "DateOfLecture": str(row[3]),
                    "Grade": row[4]
                }

                if not all_table:
                    if str(x['StudentID']) == str(student_id):
                        data_not_all.append(x)

                data_all.append(x)

            if all_table:
                return json.dumps({"json": "get grades",
                                   "code": "ok",
                                   "method": "GET",
                                   "all": "1",
                                   "data": data_all})
            else:
                return json.dumps({"json": "get grades",
                                   "code": "ok",
                                   "method": "GET",
                                   "all": "0",
                                   "data": data_not_all})
        else:
            return render_template('grades.html', Classes_names=CLASSES_NAMES, Categories=CATEGORIES)
    elif request.method == 'POST':
        if request.content_type == 'application/json':
            data = request.json
            first_name = data['first_name']
            last_name = data['last_name']
            class_ID = int(data['class_id'])
            grade = int(data['grade'])
            category_ID = int(data['category_id'])
        else:
            try:
                first_name = request.form['first_name']
                last_name = request.form['last_name']
                class_ID = int(request.form['class_id'])
                grade = int(request.form['grade'])
                category_ID = int(request.form["category_id"])
            except ValueError:
                error = 'Wrong input'
                return render_template('grades.html', error=error, Classes_names=CLASSES_NAMES, Categories=CATEGORIES)

        students, _ = get_table('Students')

        for student in students:
            if student[1] == first_name and student[2] == last_name:
                group_id = int(student[3])
                student_id = int(student[0])

        if group_id == -1 or student_id == -1:
            if request.content_type == 'application/json':
                return json.dumps({"json": "grades",
                                   "code": "error",
                                   "cause": "group or student do not exists",
                                   "method": "POST"})
            else:
                error = 'Wrong input: group or student do not exists'
                return render_template('grades.html', error=error, Classes_names=CLASSES_NAMES, Categories=CATEGORIES)

        occurrences, _ = get_table('occurence')

        for occ in occurrences:
            if occ[3] == group_id and occ[4] == category_ID and occ[5] == class_ID:
                occurrence_id = occ[0]

        if occurrence_id == -1:
            if request.content_type == 'application/json':
                return json.dumps({"json": "grades",
                                   "code": "error",
                                   "cause": "occurrence does not exists",
                                   "method": "POST"})
            else:
                error = 'occurrence does not exists'
                return render_template('grades.html', error=error, Classes_names=CLASSES_NAMES, Categories=CATEGORIES)

        connection = connect()
        cursor = connection.cursor()
        query = open(os.getcwd() + '\\queries\\insert_grade.sql', mode='r', encoding='utf-8').read(-1)
        query = query + '(' + str(student_id) + ', ' + str(occurrence_id) + ', CAST(\'' + \
                str(datetime.datetime.today().strftime('%Y-%m-%d')) + '\' as datetime), ' + str(grade) + ')'

        cursor.execute(query)
        connection.commit()
        connection.close()

        if request.content_type == 'application/json':
            return json.dumps({"json": "grades",
                               "code": "ok",
                               "method": "POST"})
        else:
            return render_template('grades.html', Classes_names=CLASSES_NAMES, Categories=CATEGORIES)

        error = 'ok'
        return render_template('grades.html', error=error, Classes_names=CLASSES_NAMES, Categories=CATEGORIES)
    elif request.method == 'DELETE':
        if request.content_type == 'application/json':
            data = request.json
            delete_all = bool(int(data['all']))
            try:
                connection = connect()
                cursor = connection.cursor()
                cursor.execute("DELETE FROM Exams where ExamID >= 1")
                connection.commit()
                connection.close()
            except pyodbc.Error:
                return json.dumps({"json": "delete grades",
                                   "code": "error",
                                   "cause": "Database error",
                                   "method": "DELETE"})

        return json.dumps({"json": "delete grades",
                           "code": "ok",
                           "method": "DELETE"})


"""
    /show_attendance endpoint
    show attendance
"""
@app.route('/show_attendance', methods=['GET', 'POST'])
def show_attendance():
    if request.method == 'GET':
        return render_template('show_attendance.html', values=[])
    elif request.method == 'POST':
        try:
            print(request.form)
            student_f_name = str(request.form.get("s_fname"))
            student_s_name = str(request.form.get("s_sname"))
            class_name = str(request.form.get("Class"))
        except ValueError:
            error = 'Wrong input'
            return render_template('attendance.html', error=error)

        query_file = open(os.getcwd() + '\\queries\\check_attendance.sql', mode='r', encoding='utf-8')
        connection = connect()
        cursor = connection.cursor()

        if student_f_name and student_s_name and class_name:
            query = query_file.read(-1) + ' and S.FirstName = \'' + str(student_f_name) + '\' and S.LastName = \'' + \
                    str(student_s_name) + '\' and C.ClassName = \'' + str(class_name) + '\''
        elif student_f_name and student_s_name:
            query = query_file.read(-1) + ' and S.FirstName = \'' + str(student_f_name) + '\' and S.LastName = \'' + \
                    str(student_s_name) + '\''
        elif student_f_name:
            query = query_file.read(-1) + ' and S.FirstName = \'' + str(student_f_name) + '\''
        elif student_s_name:
            query = query_file.read(-1) + ' and S.LastName = \'' + str(student_s_name) + '\''
        elif class_name:
            query = query_file.read(-1) + ' and C.ClassName = \'' + str(class_name) + '\''
        else:
            query = query_file.read(-1)

        cursor.execute(query)
        data = cursor.fetchall()
        if data:
            return render_template('show_attendance.html', value=data)
        else:
            error = "Wrong input"
            return render_template('show_attendance.html', error=error)


"""
    /show_if_passed endpoint
    show students that passed
"""
@app.route('/show_if_passed', methods=['GET', 'POST'])
def show_if_passed():
    if request.method == 'GET':
        return render_template('show_passing.html', value=[])
    elif request.method == 'POST':
        try:
            class_name = str(request.form.get("Class"))
        except ValueError:
            error = 'Wrong input'
            return render_template('show_passing.html', error=error)

        connection = connect()
        cursor = connection.cursor()
        query_file = open(os.getcwd() + '\\queries\\check_passing.sql', mode='r', encoding='utf-8')

        query = str(query_file.read(-1))

        cursor.execute(query, class_name)
        data = cursor.fetchall()
        print(data)
        if data:
            return render_template('show_passing.html', value=data)
        else:
            error = 'Wrong input'
            return render_template('show_passing.html', error=error)


@app.route('/show_grades', methods=['GET', 'POST'])
def show_grades():
    if request.method == 'GET':
        return render_template('show_grades.html', value=[])
    elif request.method == 'POST':
        try:
            class_name = str(request.form.get("Class"))
            student_f_name = str(request.form.get("s_fname"))
            student_s_name = str(request.form.get("s_sname"))
        except ValueError:
            error = 'Wrong input'
            return render_template('show_grades.html', error=error)

        connection = connect()
        cursor = connection.cursor()
        query_file = open(os.getcwd() + '\\queries\\check_grades.sql', mode='r', encoding='utf-8')
        if class_name and student_f_name and student_s_name:
            query = query_file.read(-1) + ' and C.ClassName = \'' + class_name + '\' and S.FirstName = \'' + \
                    student_f_name + '\' and S.LastName = \'' + student_s_name + '\''
        elif student_f_name and student_s_name:
            query = query_file.read(-1) + ' and S.FirstName = \'' + student_f_name + '\' and S.LastName = \'' + \
                    student_s_name + '\''
        elif student_f_name:
            query = query_file.read(-1) + ' and S.FirstName = \'' + str(student_f_name) + '\''
        elif student_s_name:
            query = query_file.read(-1) + ' and S.LastName = \'' + str(student_s_name) + '\''
        elif class_name:
            query = query_file.read(-1) + ' and C.ClassName = \'' + class_name + '\''

        cursor.execute(query)
        data = cursor.fetchall()
        if data:
            return render_template('show_grades.html', value=data)
        else:
            error = "Wrong input"
            return render_template('show_grades.html', error=error)

"""
    /json endpoint 
    endpoint to test
"""
@app.route('/json', methods=['GET', 'POST', 'PUT', 'DELETE'])
def get_json():
    if request.content_type == 'application/json':
        data = request.json
        print(data)
        return json.dumps({"ok": "json"})
    elif request.content_type == 'text/html':
        return render_template('index.html')

    print(request.mimetype)
    return render_template('grades.html')


if __name__ == '__main__':
    app.run()
