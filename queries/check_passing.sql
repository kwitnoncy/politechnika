--  SAMPLE:
--     with t as(
--         select S.StudentID, Count(*) Classes_attended from Students S, Attendance A, Classes C
--         where S.StudentID = A.StudentID and A.ClassID = C.ClassID and c.ClassName='Kompilatory'
--         GROUP BY S.StudentID
--     )
--
--     select  S.FirstName, S.LastName, E.Grade from Students S, Exams E, t T, Classes C
--     where S.StudentID = E.StudentID and S.StudentID=T.StudentID and E.ClassID=C.ClassID and
--           E.Grade > 2 and  T.Classes_attended > C.HourCount*0.51 GROUP BY E.Grade, S.FirstName, S.LastName

with t as(
    select S.StudentID, Count(*) Classes_attended, C.ClassName from Students S, Attendance A, Classes C
    where S.StudentID = A.StudentID and A.ClassID = C.ClassID
    GROUP BY S.StudentID, C.ClassName
)

select  S.FirstName, S.LastName, E.Grade from Students S, Exams E, t T, Classes C
where S.StudentID = E.StudentID and S.StudentID=T.StudentID and E.ClassID=C.ClassID and
      E.Grade > 2 and  T.Classes_attended > C.HourCount*0.51 and C.ClassName = ? GROUP BY E.Grade, S.FirstName, S.LastName