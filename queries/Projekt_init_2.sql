USE Politechnika;

DROP TABLE Diplomas
DROP TABLE Exams
DROP TABLE Attendance
DROP TABLE Students
DROP TABLE Occurences
DROP TABLE Classes
DROP TABLE Lecturers
DROP TABLE Groups
DROP TABLE Categories
DROP TABLE Majors
DROP TABLE Faculties


CREATE TABLE Faculties (
	FacultyID int IDENTITY(1,1),
	FacultyName varchar(50) NOT NULL,

	PRIMARY KEY (FacultyID)
);


CREATE TABLE Majors(
	MajorID int IDENTITY(1,1),
	MajorName varchar(50) NOT NULL,
	FacultyID int NOT NULL,

	PRIMARY KEY (MajorID),
    FOREIGN KEY (FacultyID) REFERENCES Faculties(FacultyID),
);

CREATE TABLE Groups(
	GroupID int IDENTITY(1,1),
	GroupName varchar(2),
	MajorID int NOT NULL,

	PRIMARY KEY (GroupID),
    FOREIGN KEY (MajorID) REFERENCES Majors(MajorID),
);

CREATE TABLE Categories(
	CategoryID int IDENTITY(1,1),
	CategoryName varchar(15) NOT NULL,
	Description ntext,

	PRIMARY KEY (CategoryID)
);

CREATE TABLE Students(
	StudentID int IDENTITY(1,1),
	FirstName varchar(20) NOT NULL,
	LastName varchar(30) NOT NULL,
	GroupID int NOT NULL,
	CardID varchar(4) NOT NULL,
	Year int NOT NULL,

	PRIMARY KEY (StudentID),
    FOREIGN KEY (GroupID) REFERENCES Groups(GroupID),
);

CREATE TABLE Lecturers (
	LecturerID int IDENTITY(1,1),
	FirstName varchar(20) NOT NULL,
	LastName varchar(30) NOT NULL,
	FacultyID int,
	LecturerCardID varchar(4),

	PRIMARY KEY (LecturerID),
    FOREIGN KEY (FacultyID) REFERENCES Faculties(FacultyID),
);

CREATE TABLE Classes(
	ClassID int IDENTITY(1,1),
	ClassName varchar(30) NOT NULL,
	MajorID int NOT NULL,

	PRIMARY KEY (ClassID),
    FOREIGN KEY (MajorID) REFERENCES Majors(MajorID),
);

CREATE TABLE Occurences(
	OccurenceID int IDENTITY(1,1),
	Day int NOT NULL CHECK (Day > 0 AND Day < 6),
	Hour int NOT NULL CHECK (Hour > 0 AND Hour < 8),
	GroupID int NOT NULL,
	CategoryID int NOT NULL,
	ClassID int NOT NULL,
	LecturerID int NOT NULL,

	PRIMARY KEY (OccurenceID),
    FOREIGN KEY (GroupID) REFERENCES Groups(GroupID),
    FOREIGN KEY (ClassID) REFERENCES Classes(ClassID),
    FOREIGN KEY (CategoryID) REFERENCES Categories(CategoryID),
    FOREIGN KEY (LecturerID) REFERENCES Lecturers(LecturerID),
);

CREATE TABLE Exams (
	ExamID int IDENTITY(1,1),
	StudentID int NOT NULL,
	OccurenceID int NOT NULL,
	DateOfLecture date NOT NULL,
	Grade int CHECK (Grade > 1 AND Grade < 6),

	PRIMARY KEY (ExamID),
    FOREIGN KEY (StudentID) REFERENCES Students(StudentID),
    FOREIGN KEY (OccurenceID) REFERENCES Occurences(OccurenceID),
);

CREATE TABLE Attendance (
	AttendanceID int IDENTITY(1,1),
	StudentID int NOT NULL,
	OccurenceID int NOT NULL,
	DateOfOccurence datetime NOT NULL,

	PRIMARY KEY (AttendanceID),
    FOREIGN KEY (StudentID) REFERENCES Students(StudentID),
    FOREIGN KEY (OccurenceID) REFERENCES Occurences(OccurenceID),
);

CREATE TABLE Diplomas(
	DiplomaID int IDENTITY(1, 1),
	Topic varchar(200),
	StudentID int NOT NULL,
	LecturerID int NOT NULL,
	FacultyID int NOT NULL,
	
	PRIMARY KEY (DiplomaID),
    FOREIGN KEY (StudentID) REFERENCES Students(StudentID),
    FOREIGN KEY (LecturerID) REFERENCES Lecturers(LecturerID),
    FOREIGN KEY (FacultyID) REFERENCES Faculties(FacultyID),
)

----------------------------------------------------------

INSERT INTO Faculties (FacultyName)
VALUES ('Elektryczny'),
	('Informatyki'),
	('Budowy Maszyn');

INSERT INTO Majors (MajorName, FacultyID)
VALUES ('Informatyka', 1), ('Automatyka i robotyka', 1),
	('Matematyka', 2),
	('Mechanika', 3);

INSERT INTO Groups (GroupName, MajorID)
VALUES ('L1', 1), ('L2', 1), ('L3', 1),
		('L1', 2), ('L1', 2), ('L1', 2);

INSERT INTO Students (FirstName, LastName, CardID, GroupID, Year)
VALUES ('Piotr', 'Kwiatkowski', CONVERT(varchar(4), 0x0367FD29), 1, 1),
	('Micha�', 'Nowak', CONVERT(varchar(4), 0xB529BF59), 2, 1),
	('Jakub', 'Kowalski', CONVERT(varchar(4), 0x6249FD29),3, 1),
	('Julia', 'Omachel', CONVERT(varchar(4), 0xFFFFFFFF),4, 1),
	('Natalia', 'Kowalik', CONVERT(varchar(4), 0xFFFFFFFF),5, 1),
	('Aleksandra', 'Konrad', CONVERT(varchar(4), 0xFFFFFFFF),6, 1);

INSERT INTO Lecturers (FirstName, LastName, FacultyID, LecturerCardID)
VALUES ('Jakub', 'Nowak', 1, CONVERT(varchar(4), 0xB09117A3)),
	('Tomasz', 'Kowalski', 2, CONVERT(varchar(4), 0xFFFFFFFF)),
	('Jonasz', 'Kiersztyn', 3, CONVERT(varchar(4), 0xFFFFFFFF)),
	('Micha�', 'Kowalik', 3, CONVERT(varchar(4), 0xFFFFFFFF));

INSERT INTO Categories (CategoryName, Description)
VALUES ('Wyklad', 'Wyklad'),
		('Cwiczenia', 'Cwiczenia na kartkach'),
		('Laboratorium', 'Cwiczenia na komputerach'),
		('Projekt', 'Robienie projektow');

INSERT INTO Classes (ClassName, MajorID)
VALUES ('Programowanie', 1), ('Kompilatory', 1),
		('Automatyka', 2), ('Robotyka', 2);
		
INSERT INTO Occurences (Day, Hour, GroupID, CategoryID, ClassID, LecturerID)
VALUES (1, 1, 1, 1, 1, 1), -- wyklad z programowania o 8.00 w poniedzialek z p. Jakubem Nowakiem - grupa L1
		(1, 1, 2, 1, 1, 3), -- wyklad z programowania o 8.00 w poniedzialek z p. Jakubem Nowakiem - grupa L2
		(1, 1, 3, 1, 1, 2), -- wyklad z programowania o 8.00 w poniedzialek z p. Jakubem Nowakiem - grupa L3
		(1, 2, 1, 3, 1, 2), -- loboratoria z programowania o 9.45 w poniedzialek z p. Tomaszem Kowalskim - grupa L1
		(1, 2, 2, 3, 1, 1), -- loboratoria z programowania o 9.45 w poniedzialek z p. Jakubem Nowakiem - grupa L2
		(2, 1, 3, 3, 1, 1), -- loboratoria z programowania o 8.00 we wtorek z p. Jakubem Nowakiem - grupa L3
		(3, 1, 1, 1, 2, 4), -- wyklad z kompilatorow o 8.00 w srode z p. Michalem Kowalikiem - grupa L1
		(3, 1, 2, 1, 2, 4), -- wyklad z kompilatorow o 8.00 w srode z p. Michalem Kowalikiem - grupa L2
		(3, 1, 3, 1, 2, 4), -- wyklad z kompilatorow o 8.00 w srode z p. Michalem Kowalikiem - grupa L3
		(3, 2, 1, 2, 2, 4), -- cwiczenia z kompilatorow o 9.45 w srode z p. Michalem Kowalikiem - grupa L1
		(3, 3, 2, 2, 2, 4), -- cwiczenia z kompilatorow o 11.45 w srode z p. Michalem Kowalikiem - grupa L2
		(4, 2, 3, 2, 2, 4) -- cwiczenia z kompilatorow o 9.45 w czwartek z p. Michalem Kowalikiem - grupa L3
;

INSERT INTO Attendance(StudentID, OccurenceID, DateOfOccurence)
VALUES  (1, 1, (CAST('2019-06-3' AS datetime))),
		(1, 1, (CAST('2019-06-10' AS datetime))),
		(1, 4, (CAST('2019-06-10' AS datetime))),
		(1, 7, (CAST('2019-06-12' AS datetime))),
		(1, 10, (CAST('2019-06-12' AS datetime))),
		(2, 2, (CAST('2019-06-10' AS datetime))),
		(2, 5, (CAST('2019-06-10' AS datetime))),
		(2, 8, (CAST('2019-06-12' AS datetime))),
		(2, 11, (CAST('2019-06-12' AS datetime))),
		(3, 3, (CAST('2019-06-10' AS datetime))),
		(3, 6, (CAST('2019-06-11' AS datetime))),
		(3, 9, (CAST('2019-06-12' AS datetime))),
		(3, 12, (CAST('2019-06-13' AS datetime)));

